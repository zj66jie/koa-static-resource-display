const Koa = require("koa");
const KoaStatic = require("koa-static");
const path = require("path"); //文件系统
const app = new Koa();
app.use(KoaStatic(path.join(__dirname, "./public")));

app.listen(3000);

console.log("listening on 3000");
